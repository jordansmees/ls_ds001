// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LS_DS001_MainGameMode.generated.h"

UCLASS(minimalapi)
class ALS_DS001_MainGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALS_DS001_MainGameMode();
};



