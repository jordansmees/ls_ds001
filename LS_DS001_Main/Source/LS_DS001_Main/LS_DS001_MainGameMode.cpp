// Copyright Epic Games, Inc. All Rights Reserved.

#include "LS_DS001_MainGameMode.h"
#include "LS_DS001_MainCharacter.h"
#include "UObject/ConstructorHelpers.h"

ALS_DS001_MainGameMode::ALS_DS001_MainGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
