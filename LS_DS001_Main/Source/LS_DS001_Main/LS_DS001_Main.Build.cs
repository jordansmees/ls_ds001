// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class LS_DS001_Main : ModuleRules
{
	public LS_DS001_Main(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
